package com.nabillahalda_10191061.praktikum1.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.nabillahalda_10191061.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}